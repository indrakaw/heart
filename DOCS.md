# DOCS

## Development Setup

Dependencies are installed ~~via LuaRocks, the rest are~~ via Git submodules.
Be sure ~~Lua-For-Windows and~~ Git-For-Windows are installed, if you are on
Windows.

Using [Love 11.3](https://love2d.org). 

Following [Lua Style Guide](https://github.com/Olivine-Labs/lua-style-guide).
Expect it has its own file structure.


## Design

The main developer is kinda eccentric he tries to strict directory structure
on project's root into 4 characters width.

- `data`: contains structured data files
- `game`: game specified directory
- `libs`: helpers and mostly third-parties
- `reso`: game's assets (bitmap, audio, etc)
- `test`: testing and debugging


## Building

To build, execute `build.sh`. On Windows it can be done via git-bash.exe. 

Game packing excludes development files AND `reso` directory.
`reso` directory by design, it's on parent of game directory.

Build procedures:

0. Create a directory and named it `build`, it's for finishing product.

1. Zip the game files, expect excluded ones (create a .love-file)
    1. Expected file is game.love (or game.dat or game.dat)
    2. (Optional) Zip the `reso` (will be mounted later)

2. Create an executable file (targets Windows)
    1. Download LÖVE .zip file (win32), get it extracted
    2. Append the .love file to the love.exe from extracted directory
    3. Copy required .dll files (and license.txt)
        - Love2D's license.txt renamed as Love2D_License.txt

3. Include game assets directory (copy; zipped or served as is)

Expected files on build directory:
```
build/
├── game.exe
├── SDL2.dll
├── OpenAL32.dll
├── Love2D_License.txt
├── love.dll
├── lua51.dll
├── mpg123.dll
├── msvcp120.dll
├── msvcr120.dll
└── reso/
```

More info: https://love2d.org/wiki/Game_Distribution

Current build file works up to zipping file and copy the game assets,
the rest will be addes later. Run the build by executing following:
```sh
# assuming love is on $PATH
love build/game.love

# on Windows might look like this
love.exe build\game.love

```


## Why `.sh`? Why not Makefile?

Bash shell is available on Unix-like OS. On Windows it shipped in 
Git-For-Windows as sell of `git-bash.exe`. We aren't doing C/C++, 
we are doing a simple procedure: building a game.
