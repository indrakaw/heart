-- Declare as a global
G = {}

-- Love's config
function love.conf(option)
    option.version = "11.3"
    option.console = false
    option.identity = "projekt000"
    option.window.vsync = false

    -- Do not show window, yet
    option.modules.window = false
end

-- Load debugger(s)
if (os.getenv("LOCAL_LUA_DEBUGGER_VSCODE") == "1") then
    (require("lldebugger")).start()
end
