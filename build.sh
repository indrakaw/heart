#!/bin/bash

function zip_it_up() {
    local target="${1:-./}"
    local dest="${1:-../game.love}"

    if ! command -v zip
    then
        echo "zip could not be found. Checking for powershell."
        if ! command -v powershell
        then
            echo "powershell could not be found"
            exit
        else
            powershell -nologo -noprofile -command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::CreateFromDirectory('.\', '..\game.love'); }"
        fi
    else
        zip -9 -qr ../game.love .
    fi
}

function copy_targets() {
    local target="${1:-build/src}"

    mkdir -v -p ./"${target}"

    cp -v ./*.lua ./"${target}"
    cp -rv ./{data,game,libs} ./"${target}"
}

function cleanup() {
    local target="${1:-build/src}"

    rm -rvf ./"${target}"
}

# =========================================================================

cd `dirname $0`

cleanup

rm -v ./build/game.exe
rm -v ./build/game.love
rm -v ./build/game.zip

rm -rv ./build/reso

copy_targets
cp -rv ./reso ./build

find "./build/src" -name ".git*" -exec rm -rf {} \;

cd ./build/src

zip_it_up && cd .. && rm -rf src
