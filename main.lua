local inspect = require "libs.inspect.inspect"
local log = require "libs.log.log"
local nativefs = require "libs.nativefs.nativefs"

log.outfile = "logger.log"

-- Is fused?
if love.filesystem.getInfo("reso") == nil then
    local dir = love.filesystem.getSourceBaseDirectory()
    nativefs.mount(dir .. "/reso", "reso")
end

-- These are mostly test and we aren't onto the game yet.
-- Can't be losing if the game haven't started, yet. >:v

log.info(
    inspect(
        {
            getDirectoryItems = love.filesystem.getDirectoryItems(""),
            getSourceBaseDirectory = love.filesystem.getSourceBaseDirectory(),
            getSource = love.filesystem.getSource()
        }
    )
)

local item = love.filesystem.read("reso/index.txt")

log.info(inspect(item))
